defmodule NucleotideCount do
  @nucleotides [?A, ?C, ?G, ?T]
	
  @doc """
    Counts individual nucleotides in a DNA strand.
  """
  @spec count([char], char) :: non_neg_integer
  def count(strand, nucleotide) when nucleotide in (@nucleotides) do
   strand
   |> Enum.count(&(&1 === nucleotide))
  end

  @doc """
    Returns a summary of counts in the strand by nucleotide.
  """
  @spec histogram([char]) :: map
  def histogram(strand) do
    @nucleotides |> Map.new(&{&1, count(strand, &1)}) 
  end
end
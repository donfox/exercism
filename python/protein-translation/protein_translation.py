#!/usr/local/bin/python3.6
# Translate RNA sequences into proteins.

codon_translation =  {
    'AUG' : 'Methionine',
    'UUU' : 'Phenylalanine',
    'UUC' : 'Phenylalanine',
    'UUA' : 'Leucine',
    'UUG' : 'Leucine',
    'UCU' : 'Serine',
    'UCC' : 'Serine',
    'UCA' : 'Serine',
    'UCG' : 'Serine',
    'UAU' : 'Tyrosine',
    'UAC' : 'Tyrosine',
    'UGU' : 'Cysteine',
    'UGC' : 'Cysteine',
    'UGG' : 'Tryptophan',
    'UAA' : 'STOP',
    'UAG' : 'STOP',
    'UGA' : 'STOP'
}
 
codon_lst = ['AUG', 'UUU', 'UUC', 'UUA', 'UUG', 'UCU', 'UCC', 'UCA', 'UCG',
             'UCG', 'UAU', 'UGU', 'UGG', 'UGC', 'UAA', 'UAG', 'UGA']

def proteins(s):
    lst = []
    ext_strs = [''.join(x) for x in zip(*[list(s[z::3]) for z in range(3)])]
    for codon in ext_strs:
        if codon in codon_lst:
            str = ''.join(codon_translation[codon])
            if str not in lst and str != 'STOP' :
                lst.append(str)
            if str == 'STOP':
                return lst            
    return lst

print(proteins('UAUUAC'))




#!/usr/local/bin/python3.6
import textwrap

plain = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
         'r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H',
         'I','J','K', 'L', 'M', 'N','O','P','Q','R','S','T','U','V','W','X',
         'Y','Z','1','2','3','4','5','6','7''8''9','0']
          
cipher = ['z','y','x','w','v','u','t','s','r','q','p','o','n','m','l','k','j',
          'i','h','g','f','e','d','c','b','a','Z','Y','X','W','V','U','T','S',
          'R','Q','P','O','N','M','L','K','J','I','H','G','F','E','D','C','B',
          'A','1','2','3','4','5','6','7','8','9','0']
              
def transform(txt):
    cln_txt = "".join(c for c in txt if c not in ('!','.',':',' ',','))
    transformed = ''
    for c in cln_txt:
        index = plain.index(c)
        transformed += cipher[index]
    return transformed


def encode(txt):
    encoded = transform(txt)
    lst_encoded = textwrap.wrap(encoded, 5) 
    str_encoded = " ".join(lst_encoded)
    encoded = str_encoded.lower()
    return encoded


def decode(txt):
    decoded = transform(txt)
    return decoded


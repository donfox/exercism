#!/usr/local/bin/python3

import math

def is_primeP(n):
    if n == 2:
        return True
    if n % 2 == 0 or n <= 1:
        return False       
    sqr = int(math.sqrt(n)) + 1
    for divisor in range(3, sqr, 2):
        if n % divisor == 0:
            return False
    return True

def get_primes(number):
    while True:
        if is_primeP(number):
            yield number
        number += 1
  
def nth_prime(n):
    if n == 0:
        raise ValueError("Size Error")
    else:
        ord = 0
        for nxt_p in get_primes(2):
            ord += 1
            if ord == n:
                return nxt_p
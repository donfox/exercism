#!/usr/local/bin/python3

# Given a string of digits, calculate the largest product for a contiguous
# substring of digits of length n.

def rec_list(l):
    if not l: return # empty list case
    # process l[0]
    return rec_list(l[1:])

def prod_list(sub_lst):
    #
    # get product of integers in sub list.
    #
    if not sub_lst: return # empty list case
    x = [int(sub_lst[j]) for j in range(len(sub_lst))]
    print("X:",x)
    return prod_list(sub_lst[1:])     



def largest_product(series, size):
    
    contig_lsts = []
    for i in range(0, len(series)-size+1):
        sub_lst = series[i:i+size]
        print(sub_lst)
        prod_list([sub_lst])
        contig_lsts.append([int(sub_lst[j]) for j in range(size)])
    print(len(contig_lsts),"Sublists", contig_lsts)

    large = 0
    for sub_lst in contig_lsts:
        prod = sub_lst[0] * sub_lst[1]
        print(sub_lst,prod)
        if prod > large:
            large = prod
    return large

series = "49142"
#print(prod_list([4, 9, 1]))
#print()
print(largest_product(series, 3))



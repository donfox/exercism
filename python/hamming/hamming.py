def distance(strand_a, strand_b):
    hamming_no = 0
    if len(strand_a) != len(strand_b):
        raise ValueError("DNA_String length mis-match:", strand_a, strand_b)  
    else:
        for n in range(len(strand_a)):
            if strand_b[n] != strand_a[n]:
                hamming_no += 1     
    return hamming_no
             

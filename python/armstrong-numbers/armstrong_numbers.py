#!/usr/local/bin/python3.6

def is_armstrong(number):
  sum = 0
  for digit in str(number):
      sum += int(digit) ** len(str(number))

  return sum == number
  
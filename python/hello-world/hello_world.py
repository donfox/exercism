#!/usr/local/bin/python3.6

def hello(name=''):
    if name == '':
        name = 'World!'
    return('Hello, ' + name)


if __name__ == '__main__':
    print(hello())
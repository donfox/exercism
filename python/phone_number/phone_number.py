class Phone(object):    
    def __init__(self, phone_number):
        cln_num = ''.join(char for char in phone_number if char in '0123456789') 
        if len(cln_num) == 11 and cln_num[0] == '1':
            cln_num = cln_num[1:]
        
        self.number = cln_num
        self.area_code = self.number[0:3]
        self.exchange_code = self.number[3:6]
        self.subscriber_num = self.number[6:10]
        
        if (len(cln_num) != 10) \
            or self.area_code[0:1] == '1' or self.area_code[0:1] == '0'\
            or self.exchange_code[0:1] == '1' or self.exchange_code[0:1] == '0':
            raise ValueError("Invalid phone number")  
            
    def pretty(self):
        return '(' + self.area_code +  ')' + ' ' + self.exchange_code + '-' + self.subscriber_num
        
def slices(series, length):
    if length <= 0 or length >len(series):
        raise ValueError("Invalid Input- Cannot slice!")
    result = []
    for i in range(0, len(series)-length+1):
        sub_lst = series[i:i+length]
        result.append([int(sub_lst[j]) for j in range(length)])
    return result
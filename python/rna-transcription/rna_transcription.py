#!/usr/local/bin/python3.6

rna_conversion = {
    'G': 'C',
    'C': 'G',
    'T': 'A',
    'A': 'U'
}

def to_rna(dna_seq):
    base = [base for base in dna_seq if base not in 'ACGT']
    if len(base) > 0:
        raise ValueError("Invalid DNA_String!", dna_seq)
    else:
        return ''.join(rna_conversion[base] for base in dna_seq)

print(to_rna('C')) # 'G'
print(to_rna('ACGTGGTCTTAA'))  # 'UGCACCAGAAUU'
print(to_rna('ACTG')) # 'UGAC'
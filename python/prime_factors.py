import math

def prime_factors(num):
    lst = []
    # the number of two's that divide num
    while num % 2 == 0:
        lst.append(2)
        num = num / 2
         
    # n must be odd so a skip of 2 ( i = i + 2) can be used
    for i in range(3,int(math.sqrt(num))+1,2):
        # while i divides n, divide n
        while num % i== 0:
            lst.append(i)
            num = num / i
             
    #  if n is a prime number greater than 2
    if num > 2:
        lst.append(num)
    return lst


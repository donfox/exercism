#!/usr/local/bin/python3.6

def rebase(from_base, digits, to_base):
    if from_base < 2 or to_base < 2:
        raise ValueError('bases must positive and larger than 1!')
    if digits and not all(0 <= digit < from_base for digit in digits):
        raise ValueError('digits must be between 0 and "from_base"')
        
    n = sum(d * from_base ** e for d, e in zip(digits, range(len(digits) - 1, -1, -1)))
    
    rebased = [] 
       
    while n:
        n, r = divmod(n, to_base)
        rebased.insert(0, r)
    return rebased